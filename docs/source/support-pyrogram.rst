Support Boter
================

Boter is free and open source software, and thus powered by your love and support! If you like the project and have
found it to be useful, give **Boter** a ``Star on Bitbucket``. Your appreciation means a lot and helps staying motivated.


