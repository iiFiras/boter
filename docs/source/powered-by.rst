Powered by Boter
===================

This is a collection of remarkable projects made with Boter.

.. A collection of Pyrojects :^)

.. tip::

    If you'd like to propose a project that's worth being listed here, feel free to open a `Feature Request`_.

Projects Showcase
-----------------

`YTAudioBot <https://t.me/ytaudio>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **A YouTube audio downloader on Telegram, serving over 200k MAU.**
| --- by `Firas <https://t.me/firas>`_

- Main: https://t.me/ytaudiobot
- Mirror: https://t.me/ytaudio_bot
- Website: https://ytaudiobot.ml

-----

`Boter Assistant <https://github.com/boter/assistant>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **The assistant bot that helps people with Boter directly on Telegram**
| --- by `Firas <https://t.me/firas>`_

- Bot: https://t.me/boterbot
- Source Code: https://github.com/boter/assistant

-----

`PyroBot <https://git.colinshark.de/PyroBot/PyroBot>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **A Telegram userbot based on Boter**
| --- by `Colin <https://t.me/ColinShark>`_

- Source Code: https://git.colinshark.de/PyroBot/PyroBot

-----

`TgIntegration <https://github.com/JosXa/tgintegration>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **Integration Test Library for Telegram Messenger Bots in Python**
| --- by `JosXa <https://t.me/JosXa>`_

- Source Code: https://github.com/JosXa/tgintegration

-----

`BotListBot <https://t.me/botlist>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| **A bot which partly uses Boter to check if other bots are still alive**
| --- by `JosXa <https://t.me/JosXa>`_

- Source Code: https://github.com/JosXa/BotListBot

-----

.. _Feature Request: https://bitbucket.org/iiFiras/boter/issues/new?labels=enhancement&template=feature_request.md

