## Boter

``` python
from boter import Client, Filters

app = Client("my_account")


@app.on_message(Filters.private)
def hello(client, message):
    message.reply_text("Hello {}".format(message.from_user.first_name))


app.run()
```

**Boter** is an elegant, easy-to-use [Telegram](https://telegram.org/) client library and framework written from the
ground up in Python and C. It enables you to easily create custom apps for both user and bot identities (bot API alternative) via the [MTProto API](https://core.telegram.org/api#telegram-api).

> [Boter in fully-asynchronous mode is also available »](https://bitbucket.org/iiFiras/boter/issues/181)
>

### Features

- **Easy**: You can install Boter with pip and start building your applications right away.
- **Elegant**: Low-level details are abstracted and re-presented in a much nicer and easier way.
- **Fast**: Crypto parts are boosted up by TgCrypto, a high-performance library
  written in pure C.
- **Documented**: Boter API methods, types and public interfaces are well documented.
- **Type-hinted**: Exposed Boter types and method parameters are all type-hinted.
- **Updated**, to make use of the latest Telegram API version and features.
- **Bot API-like**: Similar to the Bot API in its simplicity, but much more powerful and detailed.
- **Pluggable**: The Smart Plugin system allows to write components with minimal boilerplate code.
- **Comprehensive**: Execute any advanced action an official client is able to do, and even more.

### Requirements

- Python 3.5.3 or higher.
- A [Telegram API key](https://boter.readthedocs.io/intro/setup#api-keys).

### Installing

``` bash
pip3 install boter
```

### Resources

- The Docs contain lots of resources to help you getting started with Boter: https://boter.readthedocs.io.
- Reading [Examples in this repository](https://bitbucket.org/iiFiras/boter/tree/master/examples) is also a good way
  for learning how Boter works.

### Contributing

Boter is brand new, and **you are welcome to try it and help make it even better** by either submitting pull
requests or reporting issues/bugs as well as suggesting best practices, ideas, enhancements on both code
and documentation. Any help is appreciated!
