# Boter - Telegram MTProto API Client Library for Python
# Copyright (C) 2017-2019 Firas Tès <https://bitbucket.org/iiFiras>
#
# This file is part of Boter.
#
# Boter is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Boter.  If not, see <http://www.gnu.org/licenses/>.

from boter.api.functions import Ping
from boter.api.types import MsgsAck, HttpWait

from boter.api.core import Message, MsgContainer, TLObject
from .msg_id import MsgId
from .seq_no import SeqNo

not_content_related = [Ping, HttpWait, MsgsAck, MsgContainer]


class MsgFactory:
    def __init__(self):
        self.seq_no = SeqNo()

    def __call__(self, body: TLObject) -> Message:
        return Message(
            body,
            MsgId(),
            self.seq_no(type(body) not in not_content_related),
            len(body)
        )
