# Boter - Telegram MTProto API Client Library for Python
# Copyright (C) 2017-2019 Firas Tès <https://bitbucket.org/iiFiras>
#
# This file is part of Boter.
#
# Boter is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Boter.  If not, see <http://www.gnu.org/licenses/>.

from typing import Callable

import boter
from ...ext import BaseClient


class OnDisconnect(BaseClient):
    def on_disconnect(self=None) -> callable:
        """Decorator for handling disconnections.

        This does the same thing as :meth:`~boter.Client.add_handler` using the :obj:`~boter.DisconnectHandler`.
        """

        def decorator(func: Callable) -> Callable:
            if isinstance(self, boter.Client):
                self.add_handler(boter.DisconnectHandler(func))

            return func

        return decorator
