# Boter - Telegram MTProto API Client Library for Python
# Copyright (C) 2017-2018 Firas Tès <https://bitbucket.org/iiFiras>
#
# This file is part of Boter.
#
# Boter is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Boter.  If not, see <http://www.gnu.org/licenses/>.

from ..object import Object

"""- :obj:`InputLocationMessageContent`
    - :obj:`InputVenueMessageContent`
    - :obj:`InputContactMessageContent`"""


class InputMessageContent(Object):
    """Content of a message to be sent as a result of an inline query.

    Boter currently supports the following types:

    - :obj:`InputTextMessageContent`
    """

    def __init__(self):
        super().__init__()

    def write(self, reply_markup):
        raise NotImplementedError
