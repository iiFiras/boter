# Boter - Telegram MTProto API Client Library for Python
# Copyright (C) 2017-2019 Firas Tès <https://bitbucket.org/iiFiras>
#
# This file is part of Boter.
#
# Boter is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Boter.  If not, see <http://www.gnu.org/licenses/>.

from .terms_of_service import TermsOfService
from .sent_code import SentCode

__all__ = ["TermsOfService", "SentCode"]
